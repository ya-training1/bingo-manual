# Bingo Manual

## Разведка

- Для начала определимся что умеет бинарь: ```bingo -h```
- Сохраняем настройки по умолчанию и именяем их в соответствии с нашими реалиями (база, почта): ```bingo print_defautl_config > config.yaml```
- Запускаем ```strace bingo run_server``` и видим что настройки программа ищет в ```/opt/bingo/config.yaml``` а логи пытается писать в ```/opt/bongo/logs/4cd142afd1/main.log```
- Создаем/копируем необходимые файлы и папки (config.yaml, main.log)
- Поднимаем базу данных
- Наполняем БД с помощью ```bingo prepare_db```
- Снова запускаем ```bingo run_server```
- Для выяснения порта на котором слушает сервер смотрим ```lsof -p <bingo-PID>``` 
- Там же замечаем попытку подключения к DNS Google на http порт, до провала которой сервер не поднимается
- Блокируем доступ к этому порту: ```sudo iptables -I OUTPUT -p tcp -d 8.8.8.8 --dport 80 -j REJECT```

## Запуск в отказоустойчивом режиме

- Запускать будем с помощью docker-compose, для этого необходимо написать Dockerfile:
	- При запуске может ругатся на root права решаем с помощью: ```RUN adduser -D myuser``` и ```USER myuser```
	- Чтобы контейнер не раздувался логами используем ```RUN ln -sf /dev/stdout /opt/bongo/logs/4cd142afd1/main.log```
	- Добавляем curl для healthcheck 
- Для перезапуска наших нод при падении используем healthcheck и контейнер ```willfarrell/autoheal:latest```
- Для автоматизации наполнения БД разово запускаем контейнер bingo prepare_db после инициализации контейнера postgres
- Для балансировки нагрузки запускаем контейнер nginx c балансировкой между нодами bingo

## Отладка

После тестирования видим, что не все SLA выполняются, предполагаем медленную работу БД, добавляем индексы к таблицам.

## Звездочки

- Добавляем кэширование long_dummy с помощью конфига nginx: ```proxy_cache_path, proxy_cache, proxy_cache_valid, proxy_cache_bypass```
- Создаем самоподисанный сертификат:  ```openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-selfsigned.key -out nginx-selfsigned.crt```
- Добавляем настройку ssl в конфиг nginx

## Тестирование 

Для тестирования Петей поднимаем два ssh туннеля до VPS с пробросом http и https портов

