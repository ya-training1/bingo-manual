FROM alpine:latest
WORKDIR /app
RUN adduser -D myuser && chown -R myuser /app
RUN mkdir -p /opt/bongo/logs/4cd142afd1/ 
RUN ln -sf /dev/stdout /opt/bongo/logs/4cd142afd1/main.log
COPY config.yaml /opt/bingo/
RUN chown -R myuser /opt/bongo
RUN apk add curl
ADD https://storage.yandexcloud.net/final-homework/bingo /app
RUN chown -R myuser /app/bingo && chmod +x /app/bingo
USER myuser
CMD ["/app/bingo", "run_server"]

